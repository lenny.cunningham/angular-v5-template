(function(e, a) {
  for (var i in a) e[i] = a[i];
})(
  this,
  /******/ (function(modules) {
    // webpackBootstrap
    /******/ // The module cache
    /******/ var installedModules = {}; // The require function
    /******/
    /******/ /******/ function __webpack_require__(moduleId) {
      /******/
      /******/ // Check if module is in cache
      /******/ if (installedModules[moduleId]) {
        /******/ return installedModules[moduleId].exports;
        /******/
      } // Create a new module (and put it into the cache)
      /******/ /******/ var module = (installedModules[moduleId] = {
        /******/ i: moduleId,
        /******/ l: false,
        /******/ exports: {}
        /******/
      }); // Execute the module function
      /******/
      /******/ /******/ modules[moduleId].call(
        module.exports,
        module,
        module.exports,
        __webpack_require__
      ); // Flag the module as loaded
      /******/
      /******/ /******/ module.l = true; // Return the exports of the module
      /******/
      /******/ /******/ return module.exports;
      /******/
    } // expose the modules object (__webpack_modules__)
    /******/
    /******/
    /******/ /******/ __webpack_require__.m = modules; // expose the module cache
    /******/
    /******/ /******/ __webpack_require__.c = installedModules; // define getter function for harmony exports
    /******/
    /******/ /******/ __webpack_require__.d = function(exports, name, getter) {
      /******/ if (!__webpack_require__.o(exports, name)) {
        /******/ Object.defineProperty(exports, name, {
          /******/ configurable: false,
          /******/ enumerable: true,
          /******/ get: getter
          /******/
        });
        /******/
      }
      /******/
    }; // getDefaultExport function for compatibility with non-harmony modules
    /******/
    /******/ /******/ __webpack_require__.n = function(module) {
      /******/ var getter =
        module && module.__esModule
          ? /******/ function getDefault() {
              return module["default"];
            }
          : /******/ function getModuleExports() {
              return module;
            };
      /******/ __webpack_require__.d(getter, "a", getter);
      /******/ return getter;
      /******/
    }; // Object.prototype.hasOwnProperty.call
    /******/
    /******/ /******/ __webpack_require__.o = function(object, property) {
      return Object.prototype.hasOwnProperty.call(object, property);
    }; // __webpack_public_path__
    /******/
    /******/ /******/ __webpack_require__.p = ""; // Load entry module and return exports
    /******/
    /******/ /******/ return __webpack_require__((__webpack_require__.s = 1));
    /******/
  })(
    /************************************************************************/
    /******/ [
      /* 0 */
      /***/ function(module, exports) {
        module.exports = require("firebase-functions");

        /***/
      },
      /* 1 */
      /***/ function(module, exports, __webpack_require__) {
        "use strict";

        Object.defineProperty(exports, "__esModule", { value: true });
        const functions = __webpack_require__(0);
        const admin = __webpack_require__(2);
        admin.initializeApp(functions.config().firebase);
        /// Organize cloud functions based on logical roles
        const seo = __webpack_require__(3);
        /// Export functions you want deployed
        exports.app = seo.seoRouter;

        /***/
      },
      /* 2 */
      /***/ function(module, exports) {
        module.exports = require("firebase-admin");

        /***/
      },
      /* 3 */
      /***/ function(module, exports, __webpack_require__) {
        "use strict";

        Object.defineProperty(exports, "__esModule", { value: true });
        const functions = __webpack_require__(0);
        const express = __webpack_require__(4);
        const fetch = __webpack_require__(5);
        const url = __webpack_require__(6);
        const app = express();
        // You might instead set these as environment variables
        // I just want to make this example explicitly clear
        const appUrl = "template-e70bd.firebaseapp.com";
        const renderUrl = "https://render-tron.appspot.com/render";
        // Deploy your own instance of Rendertron for production
        // const renderUrl = 'your-rendertron-url';
        // Generates the URL
        function generateUrl(request) {
          return url.format({
            protocol: request.protocol,
            host: appUrl,
            pathname: request.originalUrl
          });
        }
        // List of bots to target, add more if you'd like
        function detectBot(userAgent) {
          const bots = [
            // search engine crawler bots
            "googlebot",
            "bingbot",
            "yandexbot",
            "duckduckbot",
            "slurp",
            // social media link bots
            "twitterbot",
            "facebookexternalhit",
            "linkedinbot",
            "embedly",
            "baiduspider",
            "pinterest",
            "slackbot",
            "vkShare",
            "facebot",
            "outbrain",
            "W3C_Validator"
          ];
          // Return true if the user-agent header matches a bot namespace
          const agent = userAgent.toLowerCase();
          for (const bot of bots) {
            if (agent.indexOf(bot) > -1) {
              console.log("bot detected", bot, agent);
              return true;
            }
          }
          console.log("no bots found");
          return false;
        }
        app.get("*", (req, res) => {
          const isBot = detectBot(req.headers["user-agent"]);
          if (isBot) {
            const botUrl = generateUrl(req);
            // If Bot, fetch url via rendertron
            fetch(`${renderUrl}/${botUrl}`)
              .then(res => res.text())
              .then(body => {
                // Set the Vary header to cache the user agent, based on code from:
                // https://github.com/justinribeiro/pwa-firebase-functions-botrender
                res.set("Cache-Control", "public, max-age=300, s-maxage=600");
                res.set("Vary", "User-Agent");
                res.send(body.toString());
              });
          } else {
            // Not a bot, fetch the regular Angular app
            // This is not an infinite loop because Firebase Hosting Priorities dictate index.html will be loaded first
            fetch(`https://${appUrl}`)
              .then(res => res.text())
              .then(body => {
                res.send(body.toString());
              });
          }
        });
        exports.seoRouter = functions.https.onRequest(app);

        /***/
      },
      /* 4 */
      /***/ function(module, exports) {
        module.exports = require("express");

        /***/
      },
      /* 5 */
      /***/ function(module, exports) {
        module.exports = require("node-fetch");

        /***/
      },
      /* 6 */
      /***/ function(module, exports) {
        module.exports = require("url");

        /***/
      }
      /******/
    ]
  )
);
