import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
admin.initializeApp(functions.config().firebase);

/// Organize cloud functions based on logical roles
import * as seo from "./seo";

/// Export functions you want deployed
export const app = seo.seoRouter;
