import { Component, OnInit, ViewChild } from "@angular/core";
import { NavigationStart, Router } from "@angular/router";

import { MatSidenav } from "@angular/material";
import { SidenavService } from "./core/sidenav.service";
import { AuthService } from "./auth/auth.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  @ViewChild("sidenav") public sidenav: MatSidenav;

  constructor(
    public auth: AuthService,
    private router: Router,
    private sidenavService: SidenavService
  ) {}

  ngOnInit(): void {
    // Store sidenav to service
    this.sidenavService.setSidenav(this.sidenav);

    this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.sidenavService.close().then(() => {});
      }
    });
  }

  signOut(): void {
    this.auth.signOut();
  }
}
