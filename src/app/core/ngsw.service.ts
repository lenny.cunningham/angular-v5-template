import { Injectable } from "@angular/core";

import { MatSnackBar } from "@angular/material";
import { SwUpdate } from "@angular/service-worker";

// import { interval } from 'rxjs/observable/interval';

@Injectable({
  providedIn: "root"
})
export class NgswService {
  constructor(private swUpdate: SwUpdate, private snackbar: MatSnackBar) {
    // interval(6 * 60 * 60).subscribe(() => swUpdate.checkForUpdate());

    this.swUpdate.available.subscribe(evt => {
      console.log("current version is", evt.current);
      console.log("available version is", evt.available);

      const snack = this.snackbar.open("App Update Available", "Reload");

      snack.onAction().subscribe(() => {
        this.swUpdate.activateUpdate().then(() => document.location.reload());
      });

      snack._dismissAfter(20000);
    });

    this.swUpdate.activated.subscribe(evt => {
      console.log("old version was", evt.previous);
      console.log("new version is", evt.current);
    });
  }
}
