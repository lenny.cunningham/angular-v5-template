import { Injectable } from "@angular/core";
import { Meta } from "@angular/platform-browser";

@Injectable()
export class SeoService {
  constructor(private meta: Meta) {}

  baseUrl() {
    return "https://angular-template.acadianaapps.com";
  }

  generateTags(config) {
    // default values
    config = {
      title: "Angular Material Example",
      description: "My SEO friendly Angular Material Component",
      image: `${this.baseUrl()}/assets/seo.jpeg`,
      slug: "",
      ...config
    };
    this.meta.updateTag({ name: "twitter:card", content: "summary" });
    this.meta.updateTag({ name: "twitter:site", content: "@acadianaapps" });
    this.meta.updateTag({ name: "twitter:title", content: config.title });
    this.meta.updateTag({
      name: "twitter:description",
      content: config.description
    });
    this.meta.updateTag({ name: "twitter:image", content: config.image });
    this.meta.updateTag({ property: "og:type", content: "article" });
    this.meta.updateTag({ property: "og:site_name", content: "AcadianaApps" });
    this.meta.updateTag({ property: "og:title", content: config.title });
    this.meta.updateTag({
      property: "og:description",
      content: config.description
    });
    this.meta.updateTag({ property: "og:image", content: config.image });
    this.meta.updateTag({
      property: "og:url",
      content: `${this.baseUrl()}/${config.slug}`
    });
  }
}
