import { NgModule, Optional, SkipSelf } from "@angular/core";

import { AuthService } from "../auth/auth.service";
import { NgswService } from "./ngsw.service";
import { SidenavService } from "./sidenav.service";
import { SnackbarService } from "./snackbar.service";
import { FirestoreService } from "./firestore.service";

@NgModule({
  providers: [
    AuthService,
    NgswService,
    SidenavService,
    SnackbarService,
    FirestoreService
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        "CoreModule is already loaded. Import it in the AppModule only"
      );
    }
  }
}
