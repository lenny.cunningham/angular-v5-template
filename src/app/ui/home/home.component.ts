import { Component, OnInit } from "@angular/core";
import { SeoService } from "../../core/seo/seo.service";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
  title =
    "Angular v5 Template with Firebase {Auth, Firestore} and Material Design";

  constructor(private seo: SeoService) {}

  ngOnInit() {
    this.seo.generateTags({
      title: "Home Page",
      description: this.title,
      image: `${this.seo.baseUrl()}/assets/seo.jpeg`,
      slug: "home-page"
    });
  }
}
