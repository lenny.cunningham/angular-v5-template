import { NgModule } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { SharedModule } from "../shared/shared.module";

import { HeaderComponent } from "./header/header.component";
import { HomeComponent } from "./home/home.component";

@NgModule({
  imports: [BrowserAnimationsModule, SharedModule],
  declarations: [HeaderComponent, HomeComponent],
  exports: [HeaderComponent, HomeComponent]
})
export class UiModule {}
