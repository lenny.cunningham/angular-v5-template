import { Component } from "@angular/core";

import { SidenavService } from "../../core/sidenav.service";
import { AuthService } from "../../auth/auth.service";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"]
})
export class HeaderComponent {
  constructor(
    private sidenavService: SidenavService,
    public auth: AuthService
  ) {}

  sidenavToggle() {
    this.sidenavService.toggle();
  }
}
