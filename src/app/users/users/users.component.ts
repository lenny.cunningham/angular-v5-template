import {
  AngularFirestore,
  AngularFirestoreCollection
} from "@angular/fire/firestore";
import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";

import { User } from "../user.model";
import { SeoService } from "../../core/seo/seo.service";

@Component({
  selector: "app-users",
  templateUrl: "./users.component.html",
  styleUrls: ["./users.component.css"]
})
export class UsersComponent implements OnInit {
  usersCollection: AngularFirestoreCollection<User>;
  users: Observable<User[]>;

  constructor(private seo: SeoService, private db: AngularFirestore) {
    this.usersCollection = this.db.collection<User>("users", ref =>
      ref.orderBy("displayName")
    );
    this.users = this.usersCollection.valueChanges();
  }

  ngOnInit() {
    this.seo.generateTags({
      title: "Users List Page",
      description:
        "Contact me through this awesome search engine optimized Angular component",
      image: `${this.seo.baseUrl()}/assets/seo.jpeg`,
      slug: "users-page"
    });
  }
}
