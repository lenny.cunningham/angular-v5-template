import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { AuthGuard } from "../auth/auth-guard.service";

import { UsersComponent } from "./users/users.component";
import { UserComponent } from "./user/user.component";

const usersRoutes: Routes = [
  { path: "users", canActivate: [AuthGuard], component: UsersComponent },
  { path: "user/:id", canActivate: [AuthGuard], component: UserComponent }
];

@NgModule({
  imports: [RouterModule.forChild(usersRoutes)],
  exports: [RouterModule]
})
export class UsersRoutingModule {}
