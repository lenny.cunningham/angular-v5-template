export interface Roles {
  reader?: boolean;
  editor?: boolean;
  admin?: boolean;
  responder?: boolean;
  teamLeader?: boolean;
}

export class User {
  email: string;
  photoURL: string;
  displayName: string;
  uid?: string;
  roles?: Roles;

  constructor(authData) {
    this.uid = authData.uid;
    this.email = authData.email;
    this.photoURL = authData.photoURL;
    this.displayName = authData.displayName;
    this.roles = { reader: true };
  }
}
