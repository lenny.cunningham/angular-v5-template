import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, ParamMap } from "@angular/router";

import { AngularFirestore } from "@angular/fire/firestore";

import { Observable } from "rxjs";
import { switchMap } from "rxjs/operators";

import { User } from "../user.model";
import { SeoService } from "../../core/seo/seo.service";

@Component({
  selector: "app-user",
  templateUrl: "./user.component.html",
  styleUrls: ["./user.component.css"]
})
export class UserComponent implements OnInit {
  user: Observable<User>;

  constructor(
    private seo: SeoService,
    private db: AngularFirestore,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.seo.generateTags({
      title: "User Page",
      description: "User detail page",
      image: `${this.seo.baseUrl()}/assets/seo.jpeg`,
      slug: "user-page"
    });

    this.user = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.db.doc<User>(`users/${params.get("id")}`).valueChanges()
      )
    );
  }
}
