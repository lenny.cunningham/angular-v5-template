import { Injectable } from "@angular/core";

@Injectable()
export class GeolocationService {
  constructor() {}

  getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        return [position.coords.longitude, position.coords.latitude];
      });
    }
  }

  watchLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.watchPosition(
        position => {
          // console.log('watchPositionTimestamp', position.coords);
          return position;
        },
        error => console.log(error)
      );
    }
  }
}
