import { NgModule } from "@angular/core";

import { Routes, RouterModule } from "@angular/router";
import { SharedModule } from "../shared/shared.module";

import { GeolocationComponent } from "./geolocation/geolocation.component";
import { GeolocationService } from "./geolocation.service";

// Included routing instead of separate module, currently only one page.
const routes: Routes = [
  { path: "geolocation", component: GeolocationComponent }
];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [GeolocationComponent],
  providers: [GeolocationService],
  exports: [RouterModule]
})
export class TrackingModule {}
