import { Component, OnInit, AfterViewInit } from "@angular/core";

// Init Firebase
import * as firebase from "firebase/app";
// Init GeoFireX
import * as geofirex from "geofirex";
import { toGeoJSON } from "geofirex";
import { Observable } from "rxjs";

@Component({
  selector: "app-track-user",
  templateUrl: "./geolocation.component.html",
  styleUrls: ["./geolocation.component.css"]
})
export class GeolocationComponent implements OnInit {
  geo = geofirex.init(firebase);
  points;
  geoData;
  track: Boolean = false;

  constructor() {
    this.getPoints();
  }

  ngOnInit() {
    if (navigator.geolocation && this.track) {
      navigator.geolocation.watchPosition(
        position => {
          this.unitPosition(
            position.coords.latitude,
            position.coords.longitude
          );
        },
        error => console.log(error)
      );
    }
  }

  getPoints(): void {
    // Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    // Add 'implements AfterViewInit' to the class.
    const center = this.geo.point(30, -92);
    const radius = 1000;
    const field = "position";

    const units = this.geo.collection("units").within(center, radius, field);

    this.points = units.pipe(toGeoJSON("position", true));
  }

  private showGeoData(data) {
    this.geoData = data;
  }

  private unitPosition(lat, lng) {
    const collection = this.geo.collection("units");
    collection.setPoint("ae124", "position", lat, lng);
  }
}
