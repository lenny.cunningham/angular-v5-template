import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

/*App root */
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { ServiceWorkerModule } from "@angular/service-worker";
import { environment } from "../environments/environment";

/* Database Modules */
import { AngularFireModule } from "@angular/fire";
import { AngularFirestoreModule } from "@angular/fire/firestore";
import { AngularFireStorageModule } from "@angular/fire/storage";

/*Feature Content Modules */
import { CoreModule } from "./core/core.module";
import { SharedModule } from "./shared/shared.module";
import { UiModule } from "./ui/ui.module";
import { AuthModule } from "./auth/auth.module";
import { UsersModule } from "./users/users.module";
import { MapModule } from "./map/shared/map.module";
import { FileUploadModule } from "./file-upload/shared/file-upload.module";

/*Service Modules */
import { SeoService } from "./core/seo/seo.service";
import { TrackingModule } from "./tracking/tracking.module";

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule.enablePersistence(),
    AngularFireStorageModule,
    CoreModule,
    SharedModule,
    UiModule,
    AuthModule,
    UsersModule,
    MapModule,
    TrackingModule,
    FileUploadModule,
    AppRoutingModule, // Must be last for routing provisioning
    ServiceWorkerModule.register("ngsw-worker.js", {
      enabled: environment.production
    })
  ],
  providers: [SeoService],
  bootstrap: [AppComponent]
})
export class AppModule {}
