import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { environment } from "../environments/environment";

import { HomeComponent } from "./ui/home/home.component";

const appRoutes: Routes = [
  { path: "home", component: HomeComponent },
  { path: "", redirectTo: "/home", pathMatch: "full" }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      ...(environment.production
        ? [{ enableTracing: false }]
        : [{ enableTracing: true }])
    )
  ],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule {}
