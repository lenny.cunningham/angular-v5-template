import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { SharedModule } from "../../shared/shared.module";

import { FileUploadComponent } from "../file-upload.component";
import { DropZoneDirective } from "../drop-zone.directive";
import { FileSizePipe } from "./file-size.pipe";

const routes: Routes = [
  { path: "file-upload", component: FileUploadComponent }
];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [FileUploadComponent, DropZoneDirective, FileSizePipe],
  providers: [],
  exports: [RouterModule]
})
export class FileUploadModule {}
