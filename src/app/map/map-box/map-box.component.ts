import {
  Component,
  OnInit,
  AfterViewInit,
  OnDestroy,
  ViewChild
} from "@angular/core";

import { Subject, BehaviorSubject } from "rxjs";
import {
  takeUntil,
  distinctUntilChanged,
  debounceTime,
  switchMap,
  map,
  mapTo,
  tap
} from "rxjs/operators";

import { MatSidenav } from "@angular/material";
import { SeoService } from "../../core/seo/seo.service";
import { SidenavService } from "../../core/sidenav.service";

import {
  Map,
  SymbolLayout,
  MapMouseEvent,
  LngLatBoundsLike,
  MapEvent,
  Layer,
  LngLat,
  LngLatLike,
  LngLatBounds
} from "mapbox-gl";
import * as firebase from "firebase/app";
import * as geofirex from "geofirex";
import { toGeoJSON } from "geofirex";

import { MapInstanceService } from "../shared/map-instance.service";
import { MetarsService } from "../layers/metars/metars.service";
import { MetarsLayer } from "../layers/metars/metars.model";
import { RadarLayer } from "../layers/radar/radar.service";
import { UnitsService } from "../layers/units/units.service";
import { UnitLayer } from "../layers/units/units.model";

import turfdestination from "@turf/destination";
import turfbbox from "@turf/bbox";
import turfdistance from "@turf/distance";
import { point, lineString } from "@turf/helpers";

@Component({
  selector: "app-map-box",
  templateUrl: "./map-box.component.html",
  styleUrls: ["./map-box.component.css"],
  providers: [SidenavService]
})
export class MapBoxComponent implements OnInit, OnDestroy {
  @ViewChild("mapSidenav") public mapSidenav: MatSidenav;

  // attach to pipe and destroy all subscribtions
  destroy$: Subject<boolean> = new Subject<boolean>();

  geo = geofirex.init(firebase);

  // Initalized map params
  loadedMap: Map;
  center: LngLatLike = [-92, 31];
  zoom: Array<number> = [10];
  maxBounds: LngLatBoundsLike;
  fitBounds: LngLatBounds;
  labelLayerId: string;
  selectedPoint: GeoJSON.Feature<GeoJSON.Point> | null;
  mapLayers: Layer[];
  selectedLayer: string;
  cursorStyle: string;

  private bboxArray$ = new Subject<LngLatBoundsLike>();
  private geoLocating$ = new BehaviorSubject<boolean>(false);
  private geoLocated$ = new BehaviorSubject<boolean>(false);

  // Map settings and available data
  mapStatusUpdate: string | null;
  metars;
  units;

  layouts = {
    radar: new RadarLayer(),
    metars: new MetarsLayer(),
    units: new UnitLayer()
  };

  // Turf
  bearing: number;
  distance: number;
  destination: LngLatLike;

  constructor(
    private seo: SeoService,
    private sideNavService: SidenavService,
    private mapInstanceService: MapInstanceService,
    private metarsService: MetarsService,
    private unitsService: UnitsService
  ) {}

  ngOnInit() {
    this.seo.generateTags({
      title: "Map Page",
      description: "Map detail page, utilizing mapbox-gl and Firestore",
      image: `${this.seo.baseUrl()}/assets/seo.jpeg`,
      slug: "map-page"
    });

    this.geoLocateUser();

    // Store mapSidenav to service
    this.sideNavService.setSidenav(this.mapSidenav);
  }

  private geoLocateUser() {
    if (navigator.geolocation) {
      this.geoLocating$.next(true);
      navigator.geolocation.getCurrentPosition(
        position => {
          this.center = [position.coords.longitude, position.coords.latitude];
          this.zoom = [10];
          this.geoLocated$.next(true);
        },
        error => {
          console.log(error);
          this.geoLocated$.next(false);
          this.geoLocating$.next(false);
        }
      );
    }
    this.geoLocating$.next(false);
  }

  onLoad(mapInstance: Map) {
    // Store loadedMap to component and mapInstance service
    this.loadedMap = mapInstance;
    this.mapInstanceService.mapInstance = mapInstance;

    const layers = mapInstance.getStyle().layers;
    this.mapLayers = layers;
    // Find and return label layers to put above certain layers
    for (let i = 0; i < layers.length; i++) {
      if (
        layers[i].type === "symbol" &&
        (<SymbolLayout>layers[i].layout)["text-field"]
      ) {
        this.labelLayerId = layers[i].id;
        break;
      }
    }

    this.bboxArray$
      .pipe(
        tap(bbox => console.log("bboxArrary$", bbox)),
        takeUntil(this.destroy$),
        distinctUntilChanged(),
        debounceTime(3500),
        tap(() => (this.mapStatusUpdate = "Loading Metars.....")),
        switchMap(bbox => this.metarsService.fetchMetars(bbox)),
        map(
          metarsCol =>
            (metarsCol = this.metarsService.addMetarsDistanceAndSort(
              metarsCol,
              this.loadedMap
            ))
        ),
        tap(() => (this.mapStatusUpdate = null))
      )
      .subscribe(metars => (this.metars = metars));

    this.unitsService
      .getUnits()
      .pipe(
        takeUntil(this.destroy$),
        toGeoJSON("position", true)
      )
      .subscribe(units => (this.units = units));
  }

  onClick(evt: MapMouseEvent) {
    console.log((<any>evt).features[0]);
    this.selectedPoint = (<any>evt).features[0];
  }

  moveend(moveend: MapEvent) {
    if (this.loadedMap) {
      console.log(moveend);
      this.bboxArray$.next(this.loadedMap.getBounds().toArray());
    }
  }

  toggleLayer(evt: { value: "radar" | "museums" }) {
    console.log("toggleLayer");
    this.layouts[evt.value] = {
      ...this.layouts[evt.value],
      visibility:
        this.layouts[evt.value].visibility === "visible" ? "none" : "visible"
    };
  }

  flyTo(data: any) {
    this.loadedMap.flyTo({
      center: data.geometry.coordinates
    });
  }

  getDestination() {
    const center = this.loadedMap.getCenter().toArray();
    const _dest = turfdestination(point(center), this.distance, this.bearing, {
      units: "miles"
    });
    const lngLat = _dest.geometry.coordinates;
    this.destination = lngLat;
    const planLine = lineString([center, lngLat]);
    const planBBox = turfbbox(planLine);
    this.fitBounds = new LngLatBounds(planBBox);
  }

  ngOnDestroy(): void {
    // Called once, before the instance is destroyed.
    // Add 'implements OnDestroy' to the class.
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }
}
