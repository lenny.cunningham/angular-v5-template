import { Component, OnInit, OnDestroy } from "@angular/core";

import { Subject } from "rxjs";

@Component({
  selector: "app-layers-menu",
  templateUrl: "./layers-menu.component.html",
  styleUrls: ["./layers-menu.component.css"]
})
export class LayersMenuComponent implements OnInit, OnDestroy {
  destroy$: Subject<boolean> = new Subject<boolean>();

  radarLayer;
  metarsLayer;

  constructor() {}

  ngOnInit() {}

  logLayer(_evt) {}

  ngOnDestroy(): void {
    // Called once, before the instance is destroyed.
    // Add 'implements OnDestroy' to the class.
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }
}
