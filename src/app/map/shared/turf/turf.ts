import destination from "@turf/destination";
import bbox from "@turf/bbox";

import { point, lineString } from "@turf/helpers";

export namespace turf {
  export const _turf = destination;
  export const _bbox = bbox;
  export const _point = point;
  export const _lineString = lineString;
}
