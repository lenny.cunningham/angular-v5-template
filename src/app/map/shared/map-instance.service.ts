import { Injectable } from "@angular/core";

import { Map, LngLatBoundsLike } from "mapbox-gl";
import { BehaviorSubject } from "rxjs";

@Injectable()
export class MapInstanceService {
  private _mapInstance: Map;

  private _bboxSource: BehaviorSubject<LngLatBoundsLike> = new BehaviorSubject<
    LngLatBoundsLike
  >([]);
  bbox$ = this._bboxSource.asObservable();

  constructor() {}

  /**
   * Function to update current behavior subject with current map view bounding box
   */
  bboxUpdate(): void {
    const bbox = this._mapInstance.getBounds().toArray();
    this._bboxSource.next(bbox);
  }

  /**
   * Getter for mapInstance.
   *
   * @returns {Map} loadedMap
   */
  get mapInstance(): Map {
    return this._mapInstance;
  }

  /**
   * Setter for mapInstance.
   *
   * @param {Map} loadedMap
   */
  set mapInstance(loadedMap: Map) {
    this._mapInstance = loadedMap;
  }
}
