import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { environment } from "../../../environments/environment";

import { HttpClientModule, HttpClientJsonpModule } from "@angular/common/http";

import { SharedModule } from "../../shared/shared.module";

import { NgxMapboxGLModule } from "ngx-mapbox-gl";

import { MapBoxComponent } from "../map-box/map-box.component";
import { LayersMenuComponent } from "../layers-menu/layers-menu.component";

// Map and Layer services
import { MapInstanceService } from "./map-instance.service";
import { RadarService } from "../layers/radar/radar.service";
import { MetarsService } from "../layers/metars/metars.service";
import { UnitsService } from "../layers/units/units.service";

// Included routing instead of separate module, currently only one page.
const routes: Routes = [{ path: "map", component: MapBoxComponent }];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes),
    HttpClientModule,
    HttpClientJsonpModule,
    NgxMapboxGLModule.withConfig({
      accessToken: environment.mapbox.accessToken // Can also be set per map (accessToken input of mgl-map)
    })
  ],
  declarations: [MapBoxComponent, LayersMenuComponent],
  exports: [RouterModule],
  providers: [MapInstanceService, RadarService, MetarsService, UnitsService]
})
export class MapModule {}
