import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { RasterLayout } from "mapbox-gl";

export class RadarLayer {
  layout: RasterLayout;

  constructor() {
    this.layout = {
      visibility: "visible"
    };
  }
}

@Injectable()
export class RadarService {
  // private layerSource = new BehaviorSubject({
  //   layout: {
  //     visibility: 'none'
  //   }
  // });
  // radarLayer = this.layerSource.asObservable();
  // updateLayer(layer) {
  //   this.layerSource.next(layer);
  // }
}
