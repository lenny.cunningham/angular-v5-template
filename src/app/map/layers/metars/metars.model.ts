import { Layer, SymbolPaint, SymbolLayout } from "mapbox-gl";

export interface MetarsGeoJSON {
  type: string;
  features: MetarFeature[];
}

export interface MetarFeature {
  type: string;
  properties: MetarProperties;
  geometry: MetarGeometry;
}

export interface MetarGeometry {
  type: string;
  coordinates: number[];
}

export interface MetarProperties {
  id: string;
  site: string;
  obsTime: string;
  temp: number;
  dewp: number;
  wspd: number;
  wdir: number;
  cover: string;
  visib: number;
  fltcat: string;
  altim: number;
  slp: number;
  rawOb: string;
  rawTaf: string;
}

export class MetarsLayer implements Layer {
  id: string;
  layout: SymbolLayout;
  filter;
  paint: SymbolPaint;

  constructor() {
    this.id = "metars";
    this.layout = {
      visibility: "visible",
      "text-field": "{id}",
      "text-size": 12,
      "text-transform": "uppercase",
      "icon-image": "circle-11",
      "text-offset": [0, 1]
    };
    this.filter = ["has", "id"];
    this.paint = {
      "icon-halo-color": {
        property: "fltcat",
        type: "categorical",
        stops: [
          ["VFR", "#00b33c"],
          ["MVFR", "#0066ff"],
          ["IFR", "#ff0000"],
          ["LIFR", "#ff00ff"]
        ]
      },
      "text-color": {
        property: "fltcat",
        type: "categorical",
        stops: [
          ["VFR", "#00b33c"],
          ["MVFR", "#0066ff"],
          ["IFR", "#ff0000"],
          ["LIFR", "#ff00ff"]
        ]
      },
      "text-halo-color": "#fff",
      "text-halo-width": 1
    };
  }
}
