import { Injectable } from "@angular/core";

import { throwError, Observable } from "rxjs";
import {
  HttpClient,
  HttpErrorResponse,
  HttpParams
} from "@angular/common/http";
import { catchError, retry, map } from "rxjs/operators";

import { MetarsGeoJSON } from "./metars.model";
import { LngLatBoundsLike } from "mapbox-gl";

import turfdestination from "@turf/destination";
import turfbbox from "@turf/bbox";
import turfdistance from "@turf/distance";
import { point, lineString } from "@turf/helpers";

@Injectable()
export class MetarsService {
  private readonly apiRoot =
    "https://aviationweather.gov/gis/scripts/MetarJSON.php";

  constructor(private http: HttpClient) {}

  fetchMetars(bbox: LngLatBoundsLike): Observable<MetarsGeoJSON> {
    const apiUrl = `${
      this.apiRoot
    }?jsonp=JSONP_CALLBACK&bbox=${bbox}&filter=prior&taf=1`;

    // Add safe, URL encoded parameter
    const options = bbox
      ? {
          params: new HttpParams()
            .set("jsonp", "JSONP_CALLBACK")
            .set("bbox", bbox.toString())
            .set("taf", "1")
            .set("filter", "prior")
        }
      : {};

    return this.http.jsonp<MetarsGeoJSON>(apiUrl, "JSONP_CALLBACK").pipe(
      retry(2),
      catchError(this.handleError)
    );
  }

  addMetarsDistanceAndSort(metarsCol, mapInstance) {
    metarsCol.features.forEach(function(metar) {
      const _dist = {
        distance: turfdistance(
          point(mapInstance.getCenter().toArray()),
          metar.geometry,
          { units: "miles" }
        )
      };
      metar.properties = Object.assign(metar.properties, _dist);
    });
    return this.sortMetarsByDistance(metarsCol);
  }

  sortMetarsByDistance(metarsCol) {
    metarsCol.features.sort(function(a, b) {
      if (a.properties.distance > b.properties.distance) {
        return 1;
      }
      if (a.properties.distance < b.properties.distance) {
        return -1;
      }
      // a must be equal to b
      return 0;
    });
    return metarsCol;
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An Metars error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Metars returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError(
      "Something bad happened with METARS; please try again later."
    );
  }
}
