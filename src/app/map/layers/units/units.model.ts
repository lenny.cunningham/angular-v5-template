import { SymbolLayout, SymbolPaint } from "mapbox-gl";

export interface IGeometry {
  type: string;
  coordinates: number[];
}

export interface IGeoJson {
  type: string;
  geometry: IGeometry;
  properties?: any;
  $key?: string;
}

export class UnitGeoJson implements IGeoJson {
  type = "Feature";
  geometry: IGeometry;

  constructor(coordinates, public properties?) {
    this.geometry = {
      type: "Point",
      coordinates: coordinates
    };
  }
}

export class UnitsCollection {
  type = "FeatureCollection";
  constructor(public features: Array<UnitGeoJson>) {}
}

export class UnitLayer {
  layout: SymbolLayout;
  paint: SymbolPaint;

  constructor() {
    (this.layout = {
      visibility: "visible",
      "text-field": "{unitNumber}",
      "text-size": 18,
      "text-transform": "uppercase",
      "icon-image": "{icon}-15",
      "text-offset": [0, 1]
    }),
      (this.paint = {
        "text-color": "#f16624",
        "text-halo-color": "#fff",
        "text-halo-width": 2
      });
  }
}
