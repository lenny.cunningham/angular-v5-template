import { Injectable } from "@angular/core";

// Init Firebase
import * as firebase from "firebase/app";
// Init GeoFireX
import * as geofirex from "geofirex";

@Injectable()
export class UnitsService {
  geo = geofirex.init(firebase);

  constructor() {}

  getUnits() {
    const units = this.geo.collection("units");
    return units.data();
  }
}
