import { Injectable } from "@angular/core";
import { Router } from "@angular/router";

import { Observable, of } from "rxjs";
import { switchMap } from "rxjs/operators";

/* Firebase and Angularfire */
import { AngularFireAuth } from "@angular/fire/auth";
import {
  AngularFirestore,
  AngularFirestoreDocument
} from "@angular/fire/firestore";

/* firebaseui-angular */
import { FirebaseUISignInSuccessWithAuthResult } from "firebaseui-angular";

import { User } from "./../users/user.model";

@Injectable()
export class AuthService {
  user$: Observable<User>;

  constructor(
    private afAuth: AngularFireAuth,
    private db: AngularFirestore,
    private router: Router
  ) {
    //// Get auth data, then get firestore user document || null
    this.user$ = this.afAuth.authState.pipe(
      switchMap(user => {
        if (user) {
          return this.db.doc<User>(`users/${user.uid}`).valueChanges();
        } else {
          return of(null);
        }
      })
    );
  }

  signOut() {
    this.afAuth.auth.signOut().then(() => {
      this.router.navigate(["/"]);
    });
  }

  // FirebaseUI call back for success and then update user document
  signInSuccessWithAuthResult(
    signInSuccessData: FirebaseUISignInSuccessWithAuthResult
  ) {
    this.updateUserData(signInSuccessData);
  }

  private updateUserData(
    signInSuccessData: FirebaseUISignInSuccessWithAuthResult
  ) {
    const user = signInSuccessData.authResult.user;
    const userData = new User(user);

    // Sets user data to firestore on login
    const userRef: AngularFirestoreDocument<any> = this.db.doc(
      `users/${user.uid}`
    );

    return userRef.set({ ...userData });
  }

  getUser() {
    return this.afAuth.auth.currentUser.displayName;
  }

  ///// Role-based Authorization //////
  canRead(user: User): boolean {
    const allowed = ["admin", "editor", "reader"];
    return this.checkAuthorization(user, allowed);
  }
  canEdit(user: User): boolean {
    const allowed = ["admin", "editor"];
    return this.checkAuthorization(user, allowed);
  }
  canDelete(user: User): boolean {
    const allowed = ["admin"];
    return this.checkAuthorization(user, allowed);
  }
  // determines if user has matching role
  private checkAuthorization(user: User, allowedRoles: string[]): boolean {
    if (!user) {
      return false;
    }
    for (const role of allowedRoles) {
      if (user.roles[role]) {
        return true;
      }
    }
    return false;
  }
}
