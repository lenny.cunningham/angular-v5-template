import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { AuthService } from "../../auth/auth.service";
import { FirebaseUISignInSuccessWithAuthResult } from "firebaseui-angular";

@Component({
  selector: "app-signin",
  templateUrl: "./signin.component.html",
  styleUrls: ["./signin.component.css"]
})
export class SigninComponent {
  email: string;
  password: string;

  constructor(private authService: AuthService, private router: Router) {}

  signInSuccessWithAuthResult(
    signInSuccessData: FirebaseUISignInSuccessWithAuthResult
  ) {
    this.authService.signInSuccessWithAuthResult(signInSuccessData);
    this.afterSignIn();
  }

  private afterSignIn(): void {
    this.router.navigate(["/"]);
  }
}
