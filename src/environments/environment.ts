// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBzBRbvgTAlu_povcfqmdkK8zkFWgbybuQ",
    authDomain: "angular-template.acadianaapps.com",
    databaseURL: "https://template-e70bd.firebaseio.com",
    projectId: "template-e70bd",
    storageBucket: "template-e70bd.appspot.com",
    messagingSenderId: "605117506989"
  },
  mapbox: {
    accessToken:
      "pk.eyJ1IjoibGVubnl6NzEiLCJhIjoiY2lzYnlmM290MDFhMjJ5cG5pbHYyMjJ5NiJ9.QmYija9YjNivvapqz28NkA"
  }
};
