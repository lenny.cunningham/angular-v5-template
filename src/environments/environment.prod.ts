export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyBzBRbvgTAlu_povcfqmdkK8zkFWgbybuQ",
    authDomain: "angular-template.acadianaapps.com",
    databaseURL: "https://template-e70bd.firebaseio.com",
    projectId: "template-e70bd",
    storageBucket: "template-e70bd.appspot.com",
    messagingSenderId: "605117506989"
  },
  mapbox: {
    accessToken:
      "pk.eyJ1IjoibGVubnl6NzEiLCJhIjoiY2lzYnlmM290MDFhMjJ5cG5pbHYyMjJ5NiJ9.QmYija9YjNivvapqz28NkA"
  }
};
